package com.napps.PrzegladarkaDanychzAPI;

import android.os.AsyncTask;
import android.util.Log;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.ResourceId;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.napps.PrzegladarkaDanychzAPI.BaseActivity.GOOGLE_API_KEY;


public class SearchYouTube extends AsyncTask<String, Void , Void> {

    private static YouTube youtube;
    private static final long NUMBER_OF_VIDEOS_RETURNED = 25;
    private static final String TAG = "SearchActivity";
    private List<SearchResult> searchResultList;
    private DownloadStatus mDownloadStatus;
    private List<Data> mDataList = null;
    private final OnDataAvailable mCallback;

    interface OnDataAvailable {
        void onDataAvailable(List<Data> data, DownloadStatus status);
    }

    public SearchYouTube(OnDataAvailable callback) {
        this.mDownloadStatus = DownloadStatus.IDLE;
        mCallback = callback;
    }

    @Override
    protected Void doInBackground(String... params) {
        try {

            youtube = new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), new HttpRequestInitializer() {
                public void initialize(HttpRequest request) throws IOException {
                }
            }).setApplicationName("VideoStoreAdmin").build();

            YouTube.Search.List search = youtube.search().list("id,snippet");
            search.setKey(GOOGLE_API_KEY);
            search.setQ(params[0]);
            search.setType("video");
            search.setFields("items(id/kind,id/videoId,snippet/title,snippet/description),nextPageToken");
            search.setMaxResults(NUMBER_OF_VIDEOS_RETURNED);
            SearchListResponse searchResponse = search.execute();
            searchResultList = searchResponse.getItems();
            mDownloadStatus = DownloadStatus.OK;
        } catch (GoogleJsonResponseException e) {
            System.err.println("There was a service error: " + e.getDetails().getCode() + " : "
                    + e.getDetails().getMessage());
        } catch (IOException e) {
            System.err.println("There was an IO error: " + e.getCause() + " : " + e.getMessage());
        } catch (Throwable t) {
            t.printStackTrace();
        }
        mDownloadStatus = DownloadStatus.FAILED_OR_EMPTY;
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
            if (searchResultList != null) {
                Iterator<SearchResult> iteratorSearchResults = searchResultList.iterator();
                if (!iteratorSearchResults.hasNext()) {
                    Log.d(TAG, "onQueryTextSubmit: No result");
                }
                mDataList = new ArrayList<>();
                while (iteratorSearchResults.hasNext()) {

                    SearchResult singleVideo = iteratorSearchResults.next();
                    ResourceId rId = singleVideo.getId();

                    if (rId.getKind().equals("youtube#video")) {

                        Log.d(TAG, "onQueryTextSubmit: No result" + singleVideo.getSnippet().getTitle() + "   " + rId.getVideoId() + singleVideo.getSnippet().getDescription());
                        Data dataObject = new Data(singleVideo.getSnippet().getTitle(), rId.getVideoId(), singleVideo.getSnippet().getDescription());
                        mDataList.add(dataObject);
                    }
                }
                if (mCallback != null) {
                    Log.d(TAG, "onQueryTextSubmit: callback");
                    mCallback.onDataAvailable(mDataList, DownloadStatus.OK);
                }
                super.onPostExecute(aVoid);
            }
}
}
