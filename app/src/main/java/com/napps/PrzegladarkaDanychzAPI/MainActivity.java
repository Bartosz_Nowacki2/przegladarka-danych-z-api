package com.napps.PrzegladarkaDanychzAPI;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static com.napps.PrzegladarkaDanychzAPI.R.id.navigation_movie;
import static com.napps.PrzegladarkaDanychzAPI.R.menu.menu_main;


public class MainActivity extends BaseActivity implements GetXmlData.OnDataAvailable, SearchYouTube.OnDataAvailable,
        RecyclerItemClickListener.OnRecyclerClickListener {

    private static final String TAG = "MainActivity";
    private final String URL_CONTENT = "Url content";
    private final String FAVORITES = "favorites";
    private RecyclerViewAdapter mRecyclerViewAdapter;
    private GetXmlData getXmlData;
    private List<Data> mList;
    private Intent intent = null;
    boolean inFavorite=false;
    private BaseManagement bm;
    private Data dataItem;
    private String url="https://www.youtube.com/feeds/videos.xml?playlist_id=PLScC8g4bqD47c-qHlsfhGH3j6Bg7jzFy-";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activateToolbar(false);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (savedInstanceState!=null) {
            inFavorite=savedInstanceState.getBoolean(FAVORITES);
            url=savedInstanceState.getString(URL_CONTENT);
            if (inFavorite) {
                mRecyclerViewAdapter.loadNewMovie(bm.loadData());
            } else {
                getXmlData = new GetXmlData(this);
                getXmlData.execute(url);
            }
        }else {
            getXmlData = new GetXmlData(this);
            getXmlData.execute(url);
        }

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, recyclerView, this));

        mRecyclerViewAdapter = new RecyclerViewAdapter(this, new ArrayList<Data>());
        recyclerView.setAdapter(mRecyclerViewAdapter);
        bm = new BaseManagement(this);

    }

    BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case navigation_movie:
                    setTitle(R.string.title_activity_movie_trailers);
                    inFavorite = false;
                    getXmlData = new GetXmlData(MainActivity.this);
                    url = "https://www.youtube.com/feeds/videos.xml?playlist_id=PLScC8g4bqD47c-qHlsfhGH3j6Bg7jzFy-";
                    getXmlData.execute(url);
                    return true;
                case R.id.navigation_series:
                    setTitle(R.string.title_activity_series_trailers);
                    inFavorite = false;
                    getXmlData = new GetXmlData(MainActivity.this);
                    url ="https://www.youtube.com/feeds/videos.xml?channel_id=UC1cBYqj3VXJDeUee3kMDvPg";
                    getXmlData.execute(url);
                    return true;
                case R.id.navigation_favorite:
                    setTitle(R.string.title_favorite);
                    mRecyclerViewAdapter.loadNewMovie(bm.loadData());
                    inFavorite = true;
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume starts");
        super.onResume();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String query = sharedPreferences.getString(YOUTUBE_QUERY, "");
        Log.d(TAG, "onResume " + query);

        if(query.length() > 0) {
            SearchYouTube search = new SearchYouTube(MainActivity.this);
            search.execute(query);
            Log.d(TAG, "onResume active");
        }
        Log.d(TAG, "onResume ends");
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_search) {
            Intent intent = new Intent(this, SearchActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDataAvailable(List<Data> data, DownloadStatus status) {
        if (status == DownloadStatus.OK) {
            mRecyclerViewAdapter.loadNewMovie(data);
            mList = data;
        } else {
            Log.e(TAG, "onDataAvailable failed with status " + status);
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        Toast.makeText(MainActivity.this, "Kliknij dwa razy, by przejść do szczegółów, \n Kliknij długo, by zapisać w ulubionych", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemLongClick(View view, int position) {
        if (inFavorite == false) {
            dataItem = mList.get(position);
            bm.addSlot(dataItem);
            Toast.makeText(MainActivity.this, "Trailer saved", Toast.LENGTH_SHORT).show();
        } else if (inFavorite){
            bm.removeSlot(((bm.loadData()).get(position)).getTitle());
            mRecyclerViewAdapter.loadNewMovie(bm.loadData());
            Toast.makeText(MainActivity.this, "Trailer removed", Toast.LENGTH_SHORT).show();
            getXmlData = new GetXmlData(this);
        }
    }

    @Override
    public void onDoubleTap(View view, int position) {
        intent = new Intent(this, VideoDetailActivity.class);
        intent.putExtra(VIDEO_TRANSFER, mRecyclerViewAdapter.getVideo(position));
        startActivity(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(URL_CONTENT, url);
        outState.putBoolean(FAVORITES,inFavorite);
        super.onSaveInstanceState(outState);
    }



}
