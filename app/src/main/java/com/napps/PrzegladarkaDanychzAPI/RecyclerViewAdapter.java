package com.napps.PrzegladarkaDanychzAPI;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Rewan on 2017-06-08.
 */

class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ImageViewHolder> {
    private static final String TAG = "YTRecyclerViewAdapt";
    private List<Data> mList;
    private Context mContext;

    public RecyclerViewAdapter(Context context, List<Data> dataList) {
        mContext = context;
        mList = dataList;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Called by the layout manager when it needs a new view
        Log.d(TAG, "onCreateViewHolder: new view requested");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.browse, parent, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        // Called by the layout manager when it wants new data in an existing row

        if ((mList == null) || (mList.size() == 0)) {
            holder.thumbnail.setImageResource(R.drawable.placeholder);
            holder.title.setText(R.string.empty_photo);
        } else {
            String imageUrl = "";
            Data dataItem = mList.get(position);
            Log.d(TAG, "onBindViewHolder: " + dataItem.getTitle() + " --> " + position);
            imageUrl = "https://img.youtube.com/vi/" + dataItem.getID() + "/mqdefault.jpg";

            Picasso.with(mContext).load(imageUrl)
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(holder.thumbnail);

            holder.title.setText(dataItem.getTitle());

        }
    }

    @Override
    public int getItemCount() {
        return ((mList != null) && (mList.size() != 0) ? mList.size() : 1);
    }

    void loadNewMovie(List<Data> newData) {
        mList = newData;
        notifyDataSetChanged();
    }
    public Data getVideo(int position) {
        return ((mList != null) && (mList.size() !=0) ? mList.get(position) : null);
    }

    static class ImageViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "FlickrImageViewHolder";
        ImageView thumbnail = null;
        TextView title = null;

        public ImageViewHolder(View itemView) {
            super(itemView);
            Log.d(TAG, "ImageViewHolder: starts");
            this.thumbnail = itemView.findViewById(R.id.thumbnail);
            this.title = itemView.findViewById(R.id.title);
        }
    }
}