package com.napps.PrzegladarkaDanychzAPI;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.squareup.picasso.Picasso;

public class VideoDetailActivity extends BaseActivity {
    FloatingActionButton fabAdd = null;
    ImageView thumbnail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_detail);
        fabAdd = findViewById(R.id.addFavoriteButton);
        activateToolbar(true);

        Intent intent = getIntent();
        final Data data = (Data) intent.getSerializableExtra(VIDEO_TRANSFER);
        if(data != null) {
            TextView videoTitle = findViewById(R.id.video_title);
            String text = data.getTitle();
            videoTitle.setText(text);

            TextView videoDescription = findViewById(R.id.video_description);
            videoDescription.setText(data.getDescription());

            String imageUrl = "https://img.youtube.com/vi/" + data.getID() + "/mqdefault.jpg";
            thumbnail = findViewById(R.id.video_thumbnail);
            Picasso.with(this).load(imageUrl)
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(thumbnail);
        thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = YouTubeStandalonePlayer.createVideoIntent(VideoDetailActivity.this, GOOGLE_API_KEY, data.getID(), 0, true, false);
                startActivity(intent);
            }
        });

    }

}}
